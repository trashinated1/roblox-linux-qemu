# Roblox on Linux using Qemu



## Getting started

To start, install virt-manager in your distribution.

Arch
```
sudo pacman -S qemu-desktop
```
Ubuntu/Debian
```
sudo apt-get install virt-manager qemu-system
```
Fedora
```
sudo dnf install virt-manager
```
Gentoo
```
sudo emerge virt-manager
sudo emerge app-emulation/qemu
```

## Download dependencies

The first thing you need to do is clone the repo linked below.
```
git clone https://github.com/maximilionus/android-qemu-launcher.git
```
Next, install Android x86.
https://www.fosshub.com/Android-x86.html#

You can use https://proxysite.com for faster downloads.

## Installing Android

Enter these commands:
```
cd android-qemu-launcher
./launcher.sh init
```
Press enter when prompted and when 20G shows up.

To install Android, simply do
```
./launcher.sh install [ANDROID x86 FILE]
```
and go to Advanced, then automatic install. Wait for it to install then reboot.

Enter
```
./launcher.sh
```
and complete the setup process, which includes logging into to your Google Account:
## Installing ARM translation tools and Roblox

Open Chrome in the Emulator and enter this URL.

https://tinyurl.com/rbxhou

Then, enter these commands in order.
```
cd sdcard/Download
```
```
mkdir /sdcard/arm
```
```
cp houdini.sfs /sdcard/arm/houdini9_y.sfs
```
```
su
```
```
enable_nativebridge
```
You will get a modprobe error, just enter the command twice.

After that, go to Settings -> Android x86 Options and Enable Native Bridge.

Then, download Roblox from the Google Play Store. After that, launch Roblox and start playing!
## Launch Android/Roblox

After you are done and want to launch Android again, enter this command.
```
./launcher.sh
```
# Credits
I would like to thank https://github.com/maximilionus for making this process easy.
